/*
 * File:   main.c
 */

#include <xc.h>

#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)

// must define frequency if we want delay function
#ifndef _XTAL_FREQ
#define _XTAL_FREQ 8000000 // Set crystal frequency to 8 MHz.
#endif 

// Pin definitions
#define PIN_STROBE RC4
#define PIN_0 RC0
#define PIN_1 RC1
#define PIN_2 RC2
#define PIN_3 RC3

typedef enum {
    MSG_RESET   = 0x0u,
    MSG_PING    = 0x1u,
    MSG_GET     = 0x2u,
    MSG_ACK     = 0xEu,
    MSG_NOTHING = 0xFu
} Msg;


unsigned Get_ADC(){
        // Wait for ADC to be ready
        //__delay_ms(5);
        
        // start ADC
        ADCON0bits.GO = 1;
        
        // wait until ADC is done
        while (ADCON0bits.GO);
        
        return (((unsigned)ADRESH) << 8) + (ADRESL);
}

void setBusIn() {
    TRISC |= 0x0Fu; // This tells the pin to be an input
}

void setBusOut() {
    TRISC &= 0xF0u; // This tells the pin to be an output
}

Msg readBus(){
    setBusIn();
    return (Msg) (PORTC & 0x0F);
}

void writeBus(Msg msg){
    setBusOut();
    PORTC &= 0xF0u;
    PORTC |= msg & 0x0Fu;
}

void init() {
    //Set clock frequency to 8MHz
    OSCCON |= 0x70u;
    
    // Wait for the power supply to settle
    __delay_ms(10);
    
    // CxIN pins are configured as digital
    CMCON0 |= 0x07u;

    // Set up photoresistor input pin
    TRISA0 = 1; // Set pin as input
    
    // Set up MCLR output pin
    RA2 = 1; // Set pin as high (Active low reset))
    TRISA2 = 0; // Set pin as output
    
    // Set up strobe pin
    TRISC4 = 1; // Set pin as input
    
    // Set up bus pins
    TRISC |= 0x0Fu; // Set 4 LSBs as input

    // Set only AN0 to analog
    ANSEL = 0x01u;
    
    // Configure ADC
    ADFM = 1; // Set ADC to right justified
    VCFG = 0; // Set voltage reference to Vdd
    ADCON0 &= 0b11100011u; // Choose AN0 as the ADC input
    ADON = 1; // enable ADC
    ADCON1 = 0b01010000; // set ADC clock speed to 2 us (Fosc / 16)
}

void waitForStrobe(int val) {
    while(PIN_STROBE != val);
}

void main(void)
{
    init();
    Msg chrCommand;
    int ADCResult;
    waitForStrobe(1);
    for (;;){   
        waitForStrobe(0);
        waitForStrobe(1);
        
        chrCommand = readBus();
        
        waitForStrobe(0);
        waitForStrobe(1);
        waitForStrobe(0);
        
        switch (chrCommand) {
            case MSG_RESET:
                writeBus(MSG_ACK);
                PORTAbits.RA2 = 0;
                break;
            case MSG_PING:
                writeBus(MSG_ACK);
                break;
            case MSG_GET:
                ADCResult = Get_ADC();
                for(unsigned valN = 0; valN < 3; ++valN){
                    writeBus(ADCResult >> valN * 4);
                    waitForStrobe(1);
                    waitForStrobe(0);
                }
                writeBus(MSG_ACK);
                break;
            default:
                writeBus(MSG_NOTHING);
                break;
        }

        waitForStrobe(1);
        waitForStrobe(0);
        setBusIn();
        waitForStrobe(1);
    }
    
    return;
}
